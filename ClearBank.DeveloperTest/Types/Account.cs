﻿using ClearBank.DeveloperTest.Validation;

namespace ClearBank.DeveloperTest.Types
{
    public class Account : IAccount
    {
        private readonly IAccountValidator accountValidator;

        public Account(IAccountValidator accountValidator)
        {
            this.accountValidator = accountValidator;
        }

        public string AccountNumber { get; set; }
        public decimal Balance { get; set; }
        public AccountStatus Status { get; set; }
        public AllowedPaymentSchemes AllowedPaymentSchemes { get; set; }

        public void ChangeBalanceBy(decimal amount)
        {
            Balance += amount;
        }

        public bool IsValid(MakePaymentRequest request)
        {
            return accountValidator.IsValid(this, request);
        }
    }
}
