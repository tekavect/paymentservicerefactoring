﻿namespace ClearBank.DeveloperTest.Types
{
    public interface IAccount
    {
        string AccountNumber { get; set; }
        decimal Balance { get; set; }
        AccountStatus Status { get; set; }
        AllowedPaymentSchemes AllowedPaymentSchemes { get; set; }
        void ChangeBalanceBy(decimal amount);
        bool IsValid(MakePaymentRequest request);
    }
}