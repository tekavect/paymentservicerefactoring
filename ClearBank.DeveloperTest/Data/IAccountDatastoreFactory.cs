﻿namespace ClearBank.DeveloperTest.Data
{
    public interface IAccountDatastoreFactory
    {
        IAccountDataStore CreateAccountDataStore();
    }
}