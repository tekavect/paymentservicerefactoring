﻿using System.Collections.Specialized;

namespace ClearBank.DeveloperTest.Data
{
    public class AccountDatastoreFactory : IAccountDatastoreFactory
    {
        public static string DataStoreTypeSettingKey = "DataStoreType";
        public static string BackupDataStoreType = "Backup";
        private readonly NameValueCollection appSettings;

        public AccountDatastoreFactory(NameValueCollection appSettings)
        {
            this.appSettings = appSettings;
        }

        public IAccountDataStore CreateAccountDataStore()
        {
            var dataStoreType = appSettings[DataStoreTypeSettingKey];
            if (dataStoreType == BackupDataStoreType)
            {
                return new BackupAccountDataStore();
            }
            return new AccountDataStore();
        }
    }
}