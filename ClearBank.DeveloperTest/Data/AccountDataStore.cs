﻿using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Validation;

namespace ClearBank.DeveloperTest.Data
{
    public class AccountDataStore : IAccountDataStore
    {
        public IAccount GetAccount(string accountNumber)
        {
            // Access database to retrieve account, code removed for brevity 
            return new Account(GetAccountValidator());
        }

        public void UpdateAccount(IAccount account)
        {
            // Update account in database, code removed for brevity
        }

        private static AccountValidator GetAccountValidator()
        {
            return new AccountValidator(new BacsAccountValidator(), new FasterPaymentsAccountValidator(), new ChapsAccountValidator());
        }
    }
}
