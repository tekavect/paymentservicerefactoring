﻿using System.Linq;
using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Validation
{
    public class AccountValidator : IAccountValidator
    {
        private readonly IAccountValidator[] accountValidators;

        public AccountValidator(params IAccountValidator[] accountValidators)
        {
            this.accountValidators = accountValidators;
        }

        public bool CanHandle(PaymentScheme paymentScheme)
        {
            return true;
        }

        public bool IsValid(IAccount account, MakePaymentRequest request)
        {
            foreach (var accountValidator in accountValidators.Where(a => a.CanHandle(request.PaymentScheme)))
            {
                return accountValidator.IsValid(account, request);
            }
            return false; 
        }
    }
}