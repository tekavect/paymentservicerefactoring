﻿using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Validation
{
    public class FasterPaymentsAccountValidator : IAccountValidator
    {
        public bool CanHandle(PaymentScheme paymentScheme)
        {
            return paymentScheme == PaymentScheme.FasterPayments;
        }

        public bool IsValid(IAccount account, MakePaymentRequest request)
        {
            if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.FasterPayments))
            {
                return false;
            }
            return account.Balance >= request.Amount;
        }
    }
}