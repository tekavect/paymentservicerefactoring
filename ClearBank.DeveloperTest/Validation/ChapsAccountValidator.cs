﻿using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Validation
{
    public class ChapsAccountValidator : IAccountValidator
    {
        public bool CanHandle(PaymentScheme paymentScheme)
        {
            return paymentScheme == PaymentScheme.Chaps;
        }

        public bool IsValid(IAccount account, MakePaymentRequest request)
        {
            if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Chaps))
            {
                return false;
            }
            return account.Status == AccountStatus.Live;
        }
    }
}