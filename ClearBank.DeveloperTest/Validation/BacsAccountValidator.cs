﻿using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Validation
{
    public class BacsAccountValidator : IAccountValidator
    {
        public bool CanHandle(PaymentScheme paymentScheme)
        {
            return paymentScheme == PaymentScheme.Bacs;
        }

        public bool IsValid(IAccount account, MakePaymentRequest request)
        {
            return account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Bacs);
        }
    }
}