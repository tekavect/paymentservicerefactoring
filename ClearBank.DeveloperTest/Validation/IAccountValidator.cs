﻿using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Validation
{
    public interface IAccountValidator
    {
        bool CanHandle(PaymentScheme paymentScheme);
        bool IsValid(IAccount account, MakePaymentRequest request);
    }
}