﻿using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IAccountDatastoreFactory accountDataStoreFactory;

        public PaymentService(IAccountDatastoreFactory accountDataStoreFactory)
        {
            this.accountDataStoreFactory = accountDataStoreFactory;
        }

        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var accountDataStore = accountDataStoreFactory.CreateAccountDataStore();
            var account = accountDataStore.GetAccount(request.DebtorAccountNumber);

            if (account == null || !account.IsValid(request)) return new MakePaymentResult {Success = false};

            account.ChangeBalanceBy(-request.Amount);
            accountDataStore.UpdateAccount(account);
            return new MakePaymentResult{Success = true};
        }
    }
}
