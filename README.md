# PaymentService Refactoring

### Exercise description

In the ‘PaymentService.cs’ file you will find a method for making a payment. At a high level the steps for making a payment are:

1.	Lookup the account the payment is being made from.
2.	Check that the account is in a valid state to make the payment.
3.	Deduct the payment amount from the account’s balance and update the account in the database.

What we’d like you to do is refactor the code with the following things in mind:

*	Adherence to SOLID principals
*	Testability
*	Readability

We’d also like you to add some unit tests to the ClearBank.DeveloperTest. Tests project to show how you would test the code that you’ve produced.


### Exercise rules

*	The solution should build
*	The tests should all pass
*	You should not change the method signature of the MakePayment method.
You are free to use any frameworks/NuGet packages that you see fit. 


## Refactoring notes

* Some parts of the code base could be further refactored, e.g. replacing creation (and duplication) of the AccountValidator in AccountDataStore classes. 
* The original PaymentService never hits account updating because `result.Success` could never be true. I've changed this piece of logic but in the 'real-world scenario' I'd clarify this change with other stakeholders. 
* There's no consumer of ClientService in the project (and no App.config) so I've just injected appSettings into `AccountDatastoreFactory`. I would update consumers in the real app or find an appropriate solution after seeing the actual service consumption.
* I'm deliberately breaking C# conventions for unit test methods naming as I found underscores are more readable than pascal case (especially with longer and more descriptive unit test names). 
* I separate Arrange/Act/Assert sections in unit tests with empty lines only.