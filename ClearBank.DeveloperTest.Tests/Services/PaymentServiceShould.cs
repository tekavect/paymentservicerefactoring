﻿using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Types;
using FluentAssertions;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests.Services
{
    public class PaymentServiceShould
    {
        private readonly Mock<IAccountDatastoreFactory> accountDataStoreFactory = new Mock<IAccountDatastoreFactory>();
        private readonly Mock<IAccountDataStore> accountDataStore = new Mock<IAccountDataStore>();
        private readonly Mock<IAccount> account = new Mock<IAccount>();
        private const string DebtorAccountNumber = "456";

        [Fact]
        public void return_unsuccessfull_payment_result_if_account_is_in_invalid_state()
        {
            var paymentService = SetupPaymentService();
            var request = new MakePaymentRequest {DebtorAccountNumber = DebtorAccountNumber};
            account.Setup(a => a.IsValid(request)).Returns(false);

            var result = paymentService.MakePayment(request);

            result.Success.Should().Be(false);
        }

        [Fact]
        public void deduct_balance_if_account_is_in_valid_state()
        {
            var paymentService = SetupPaymentService();
            var amount = 12345m;
            var request = new MakePaymentRequest {DebtorAccountNumber = DebtorAccountNumber, Amount = amount};
            account.Setup(a => a.IsValid(request)).Returns(true);

            paymentService.MakePayment(request);

            account.Verify(a => a.ChangeBalanceBy(-amount), Times.Once);
        }

        [Fact]
        public void update_account_if_account_is_in_valid_state()
        {
            var paymentService = new PaymentService(accountDataStoreFactory.Object);
            accountDataStoreFactory.Setup(a => a.CreateAccountDataStore()).Returns(accountDataStore.Object);
            accountDataStore.Setup(a => a.GetAccount(DebtorAccountNumber)).Returns(account.Object);
            var amount = 12345m;
            var request = new MakePaymentRequest {DebtorAccountNumber = DebtorAccountNumber, Amount = amount};
            account.Setup(a => a.IsValid(request)).Returns(true);

            paymentService.MakePayment(request);

            accountDataStore.Verify(a => a.UpdateAccount(It.IsAny<IAccount>()), Times.Once);
        }

        [Fact]
        public void return_successfull_payment_result_if_account_is_in_valid_state()
        {
            var paymentService = SetupPaymentService();
            var request = new MakePaymentRequest { DebtorAccountNumber = DebtorAccountNumber };
            account.Setup(a => a.IsValid(request)).Returns(true);

            var result = paymentService.MakePayment(request);

            result.Success.Should().Be(true);
        }


        private PaymentService SetupPaymentService()
        {
            var paymentService = new PaymentService(accountDataStoreFactory.Object);
            accountDataStoreFactory.Setup(a => a.CreateAccountDataStore()).Returns(accountDataStore.Object);
            accountDataStore.Setup(a => a.GetAccount(DebtorAccountNumber)).Returns(account.Object);
            return paymentService;
        }
    }
}