﻿using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Validation;
using FluentAssertions;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests.Validation
{
    public class ChapsAccountValidatorShould
    {
        [Theory]
        [InlineData(PaymentScheme.Bacs, false)]
        [InlineData(PaymentScheme.Chaps, true)]
        [InlineData(PaymentScheme.FasterPayments, false)]
        public void handle_chaps_payment_scheme_only(PaymentScheme paymentScheme, bool expectedHandlingAbility)
        {
            var bacsAccountValidator = new ChapsAccountValidator();

            var result = bacsAccountValidator.CanHandle(paymentScheme);

            result.Should().Be(expectedHandlingAbility);
        }

        [Theory]
        [InlineData(AllowedPaymentSchemes.Bacs, false)]
        [InlineData(AllowedPaymentSchemes.Chaps, true)]
        [InlineData(AllowedPaymentSchemes.FasterPayments, false)]
        public void be_invalid_if_account_allowed_payment_scheme_is_not_chaps_flagged(AllowedPaymentSchemes allowedPaymentSchemes, bool expectedVaildState)
        {
            var bacsAccountValidator = new ChapsAccountValidator();
            Mock<IAccountValidator> accountValidator = new Mock<IAccountValidator>();
            IAccount account =
                new Account(accountValidator.Object) { AllowedPaymentSchemes = allowedPaymentSchemes };
            MakePaymentRequest request = new MakePaymentRequest();

            var result = bacsAccountValidator.IsValid(account, request);

            result.Should().Be(expectedVaildState);
        }

        [Theory]
        [InlineData(AccountStatus.Disabled, false)]
        [InlineData(AccountStatus.InboundPaymentsOnly, false)]
        [InlineData(AccountStatus.Live, true)]
        public void be_valid_if_account_allowed_payment_scheme_is_chaps_flagged_and_acount_status_is_live(AccountStatus accountStatus, bool expectedVaildState)
        {
            var bacsAccountValidator = new ChapsAccountValidator();
            Mock<IAccountValidator> accountValidator = new Mock<IAccountValidator>();
            IAccount account =
                new Account(accountValidator.Object)
                {
                    AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps,
                    Status = accountStatus
                };
            MakePaymentRequest request = new MakePaymentRequest();

            var result = bacsAccountValidator.IsValid(account, request);

            result.Should().Be(expectedVaildState);
        }
    }
}