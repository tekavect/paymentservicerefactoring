﻿using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Validation;
using FluentAssertions;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests.Validation
{
    public class FasterPaymentsAccountValidatorShould
    {
        [Theory]
        [InlineData(PaymentScheme.Bacs, false)]
        [InlineData(PaymentScheme.Chaps, false)]
        [InlineData(PaymentScheme.FasterPayments, true)]
        public void handle_faster_payment_scheme_only(PaymentScheme paymentScheme, bool expectedHandlingAbility)
        {
            var bacsAccountValidator = new FasterPaymentsAccountValidator();

            var result = bacsAccountValidator.CanHandle(paymentScheme);

            result.Should().Be(expectedHandlingAbility);
        }

        [Theory]
        [InlineData(AllowedPaymentSchemes.Bacs, false)]
        [InlineData(AllowedPaymentSchemes.Chaps, false)]
        [InlineData(AllowedPaymentSchemes.FasterPayments, true)]
        public void be_invalid_if_account_allowed_payment_scheme_is_not_faster_payments_flagged(AllowedPaymentSchemes allowedPaymentSchemes, bool expectedVaildState)
        {
            var bacsAccountValidator = new FasterPaymentsAccountValidator();
            Mock<IAccountValidator> accountValidator = new Mock<IAccountValidator>();
            IAccount account =
                new Account(accountValidator.Object) {AllowedPaymentSchemes = allowedPaymentSchemes };
            MakePaymentRequest request = new MakePaymentRequest();

            var result = bacsAccountValidator.IsValid(account, request);

            result.Should().Be(expectedVaildState);
        }

        [Theory]
        [InlineData(0, 0, true)]
        [InlineData(0, 1, true)]
        [InlineData(1, 0, false)]
        public void be_valid_if_account_allowed_payment_scheme_is_faster_payments_flagged_and_balance_is_greater_or_equal_to_requested_amount(decimal requestedAMount, decimal accountBalance, bool expectedVaildState)
        {
            var bacsAccountValidator = new FasterPaymentsAccountValidator();
            Mock<IAccountValidator> accountValidator = new Mock<IAccountValidator>();
            IAccount account =
                new Account(accountValidator.Object)
                {
                    AllowedPaymentSchemes = AllowedPaymentSchemes.FasterPayments,
                    Balance = accountBalance
                };
            MakePaymentRequest request = new MakePaymentRequest{Amount = requestedAMount};

            var result = bacsAccountValidator.IsValid(account, request);

            result.Should().Be(expectedVaildState);
        }
    }
}