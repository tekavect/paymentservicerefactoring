﻿using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Validation;
using FluentAssertions;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests.Validation
{
    public class BacsAccountValidatorShould
    {
        [Theory]
        [InlineData(PaymentScheme.Bacs, true)]
        [InlineData(PaymentScheme.Chaps, false)]
        [InlineData(PaymentScheme.FasterPayments, false)]
        public void handle_bacs_payment_scheme_only(PaymentScheme paymentScheme, bool expectedHandlingAbility)
        {
            var bacsAccountValidator = new BacsAccountValidator();

            var result = bacsAccountValidator.CanHandle(paymentScheme);

            result.Should().Be(expectedHandlingAbility);
        }

        [Theory]
        [InlineData(AllowedPaymentSchemes.Bacs, true)]
        [InlineData(AllowedPaymentSchemes.Chaps, false)]
        [InlineData(AllowedPaymentSchemes.FasterPayments, false)]
        public void be_valid_if_account_allowed_payment_scheme_is_bacs_flagged(AllowedPaymentSchemes allowedPaymentSchemes, bool expectedVaildState)
        {
            var bacsAccountValidator = new BacsAccountValidator();
            Mock<IAccountValidator> accountValidator = new Mock<IAccountValidator>();
            IAccount account =
                new Account(accountValidator.Object) {AllowedPaymentSchemes = allowedPaymentSchemes };
            MakePaymentRequest request = new MakePaymentRequest();

            var result = bacsAccountValidator.IsValid(account, request);

            result.Should().Be(expectedVaildState);
        }
    }
}