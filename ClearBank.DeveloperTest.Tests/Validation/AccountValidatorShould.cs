﻿using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Validation;
using FluentAssertions;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests.Validation
{
    public class AccountValidatorShould
    {
        private readonly Mock<IAccount> account = new Mock<IAccount>();

        [Fact]
        public void use_only_the_validator_that_can_handle_validation()
        {
            var validator1 = new Mock<IAccountValidator>();
            var validator2 = new Mock<IAccountValidator>();
            var accountValidator = new AccountValidator(validator1.Object, validator2.Object);
            var request = new MakePaymentRequest();
            validator1.Setup(a => a.CanHandle(It.IsAny<PaymentScheme>())).Returns(true);
            validator2.Setup(a => a.CanHandle(It.IsAny<PaymentScheme>())).Returns(false);

            accountValidator.IsValid(account.Object, request);

            validator1.Verify(a => a.IsValid(account.Object, request), Times.Once);
            validator2.Verify(a => a.IsValid(account.Object, request), Times.Never);
        }

        [Theory]
        [InlineData(false, false, false)]
        [InlineData(false, true, false)]
        [InlineData(true, false, false)]
        [InlineData(true, true, true)]
        public void be_valid_if_one_of_the_children_validators_that_can_handle_validation_is_valid(bool validatorResult, bool validatorCanHandle, bool expectedValidationResult)
        {
            var validator = new Mock<IAccountValidator>();
            var accountValidator = new AccountValidator(validator.Object);
            var request = new MakePaymentRequest();
            validator.Setup(a => a.IsValid(account.Object, request)).Returns(validatorResult);
            validator.Setup(a => a.CanHandle(It.IsAny<PaymentScheme>())).Returns(validatorCanHandle);

            var result = accountValidator.IsValid(account.Object, request);

            result.Should().Be(expectedValidationResult);
        }
    }
}