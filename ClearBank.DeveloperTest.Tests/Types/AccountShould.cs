﻿using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Validation;
using FluentAssertions;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests.Types
{
    public class AccountShould
    {
        private readonly Mock<IAccountValidator> accountValidator = new Mock<IAccountValidator>();

        [Theory]
        [InlineData(false, false)]
        [InlineData(true, true)]
        public void validate_its_state(bool validatorResult, bool expextedAccountStateResult)
        {
            var account = new Account(accountValidator.Object);
            var request = new MakePaymentRequest();
            accountValidator.Setup(a => a.IsValid(It.IsAny<IAccount>(), request)).Returns(validatorResult);

            var result = account.IsValid(request);

            accountValidator.Verify(a => a.IsValid(It.IsAny<IAccount>(), request), Times.Once);
            result.Should().Be(expextedAccountStateResult);
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(-1, 0, -1)]
        [InlineData(1, 0, 1)]
        public void change_its_balance_by_a_given_amount(decimal amount, decimal initialBalance, decimal resultBalance)
        {
            var account = new Account(accountValidator.Object) {Balance = initialBalance};

            account.ChangeBalanceBy(amount);

            account.Balance.Should().Be(resultBalance);
        }
    }
}