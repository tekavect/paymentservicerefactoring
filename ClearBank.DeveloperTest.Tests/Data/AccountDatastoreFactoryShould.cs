﻿using System.Collections.Specialized;
using ClearBank.DeveloperTest.Data;
using FluentAssertions;
using Xunit;

namespace ClearBank.DeveloperTest.Tests.Data
{
    public class AccountDatastoreFactoryShould
    {
        private readonly NameValueCollection appSettingsForBackupDataStore = new NameValueCollection
        {
            {AccountDatastoreFactory.DataStoreTypeSettingKey, AccountDatastoreFactory.BackupDataStoreType}
        };
        private readonly NameValueCollection appSettingsForNonBackupDataStore = new NameValueCollection
        {
            {AccountDatastoreFactory.DataStoreTypeSettingKey, string.Empty}
        };

        [Fact]
        public void create_backup_account_datastore_if_configuration_is_set_to_backup()
        {
            var accountDatastoreFactory = new AccountDatastoreFactory(appSettingsForBackupDataStore);

            var accountDataStore = accountDatastoreFactory.CreateAccountDataStore();

            accountDataStore.Should().BeOfType<BackupAccountDataStore>();
        }

        [Fact]
        public void create_account_datastore_if_configuration_is_not_set_to_backup()
        {
            var accountDatastoreFactory = new AccountDatastoreFactory(appSettingsForNonBackupDataStore);

            var accountDataStore = accountDatastoreFactory.CreateAccountDataStore();

            accountDataStore.Should().BeOfType<AccountDataStore>();
        }
    }
}